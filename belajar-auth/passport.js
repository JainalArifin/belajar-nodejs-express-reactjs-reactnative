var passport = require('passport');
var LocalStrategy = require('passport-local');
var User = require('./models/user');

passport.serializeUser((user, done)=>{
    done(null, user._id);
})

passport.deserializeUser((id, done)=>{
    User.findById(id, (err, user)=>{
        done(err, user);
    })
})

passport.use('local-login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, function(req, email, password, done){
    User.findOne({ email:email}, (err, user) => {

            if(err){
                return done(err);
            }
        
            if(!user){
                return done(null, false);
            }
        
            // if(!user.comparePassword(password)){
            //     return done(null, false);
            // }
        
            return done(null, user);
    })
}))