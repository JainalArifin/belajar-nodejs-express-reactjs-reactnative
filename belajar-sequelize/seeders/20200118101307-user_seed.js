'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
   return queryInterface.bulkInsert('Users', [
     {
      nama: 'Jainal Arifin',
      email: 'jainalSayang@gmail.com',
      telp: 999,
      alamat: 'ds.asasasas',
      DaerahId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      nama: 'Lusy ayu',
      email: 'jainalSayangLusy@gmail.com',
      telp: 999,
      alamat: 'ds.asasasas',
      DaerahId: 2,
      createdAt: new Date(),
      updatedAt: new Date()
     }
   ], {})
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
   return queryInterface.bulkDelete('Users', null, {})
  }
};
