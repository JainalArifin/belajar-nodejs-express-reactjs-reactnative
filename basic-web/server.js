var express = require("express");
var app = express();

app.get('/', (req, res, next)=>{
    res.json("halaman bernda");
})

// app.get('/products', (req, res, next)=>{
//     res.json("ini products");
// })
// app.get('/products/:name', (req, res, next)=>{
//     res.json(req.params.name);
// })
// app.use((req, res, next) =>{
//     if(0 > 50){
//         next();
//     }else{
//         res.json('gagal ke bandung');
//     }
// })

function kondisKeuangan(req, res, next){
    if(req.params.uang === "oke"){
        next();
    }else{
        res.json("anda bisa tidak bisa ke bandung");
    }
}

app.get('/bandung/:uang', kondisKeuangan, (req, res, next) => {
    res.json("anda bisa jalan-jalan ke bandung");
})

// membuat koneksi ke server
app.listen(3000, (err)=>{
    if(err){
        console.log(err)
    }else{
        console.log('Server berjalan dengan port 3000');
    }
})