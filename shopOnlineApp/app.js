var express = require('express');
var path = require('path')

// initial app
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('viewe engine', 'ejs');

// set public folder
app.use(express.static(path.join(__dirname, 'public')));

// Home / index
app.get('/', function(req, res){
    res.send("It's works");
})

// Setup server
var PORT = 3008;

app.listen(PORT, function(){
    console.log(`Server running on PORT : ${PORT}`)
})