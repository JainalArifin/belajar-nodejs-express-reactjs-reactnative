var express = require('express');
var bodyParser = require('body-parser');
var ejs = require('ejs');
var engine = require('ejs-mate');

var app = express();
var PORT = 3009;

// setup engine
app.engine('ejs', engine);
app.set('view engine', 'ejs');


// setup midleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

// home 
app.get('/', (req, res, next) => {
    res.render('home', { name: 'jainal' });
})
// home 
app.get('/about', (req, res, next) => {
    res.render('about');
})

app.listen(PORT, (err)=>{
    if(err){
        console.log(err)
    }else{
        console.log(`server berjalan dengan port : ${PORT}`)
    }
})