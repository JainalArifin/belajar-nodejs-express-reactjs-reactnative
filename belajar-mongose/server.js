var express = require("express");
var mongoose = require("mongoose");
var bodyParser = require("body-parser");

var app = express();
var PORT = 4005;

mongoose.connect('mongodb+srv://admin:a1b2c3@mongoosecobafsd-wbfle.mongodb.net/learning?retryWrites=true&w=majority',  { 
		useNewUrlParser: true,
		useUnifiedTopology: true, // commented out currently
 }, function(err){
    if(err){
        console.log(err);
    }else{
        console.log('berhasil konek ke database');
    }
})


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())

var userSchema = new mongoose.Schema({
    name: String,
    age: Number
})
userSchema.methods.addLastName = function(lastName){
    this.name = this.name + ' ' + lastName;
    return this.name;
}

var User = mongoose.model('User', userSchema);



app.get('/', (req, res, next)=>{
    // res.json("hallo selamat datang");
    User.find()
    .then((foundUser)=>{
        res.json(foundUser)
    })
    .catch((err)=> res.json(err))
})
app.get('/:name', (req, res, next)=>{
    // res.json("hallo selamat datang");
    User.findOne({ name: req.params.name })
    .then((foundUser)=>{
        res.json(foundUser)
    })
    .catch((err)=> res.json(err))
})
app.get('/add-lastname/:name/:lastname', (req, res, next)=>{
    // res.json("hallo selamat datang");
    User.findOne({ name: req.params.name }, (err, foundUser)=>{
        foundUser.addLastName(req.params.lastname);
        foundUser.save(function(err){
            if(err) res.json(err);
            res.json(foundUser);
        })
    })
})
app.post('/create-user', (req, res, next) => {
    console.log('proses nambah berjalan...');
    
    var user = new User();
    user.name = req.body.name;
    user.age = req.body.age;
    user.save((err)=> {
        if(err) console.log(err);
        res.json(user);
    });
})

app.listen(PORT, (err)=>{
    if(err){
        console.log(err);
    }else{
        console.log(`server berjalan pada port ${PORT}`)
    }
})